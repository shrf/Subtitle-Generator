package com.shrf.sherif.subtitlegenerator;

import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.google.api.client.util.Base64;
import com.google.api.services.speech.v1beta1.Speech;
import com.google.api.services.speech.v1beta1.SpeechRequestInitializer;
import com.google.api.services.speech.v1beta1.model.RecognitionAudio;
import com.google.api.services.speech.v1beta1.model.RecognitionConfig;
import com.google.api.services.speech.v1beta1.model.SpeechRecognitionResult;
import com.google.api.services.speech.v1beta1.model.SyncRecognizeRequest;
import com.google.api.services.speech.v1beta1.model.SyncRecognizeResponse;

import org.apache.commons.io.IOUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final java.lang.String CLOUD_API_KEY = "AIzaSyCRAkXSrrRLrQoDxZhGq3T7XKR4caCzlas";

    private FFmpeg ffmpeg;
    private String filePath;

    private RelativeLayout relativeLayout;
    private TextView textView;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);


        loadFFMpegBinary();

        relativeLayout = findViewById(R.id.relativeLayout);
        textView = findViewById(R.id.textView);
        progressBar = findViewById(R.id.progressBar );

        ImageView button = findViewById(R.id.btn_slct);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent filePicker = new Intent(Intent.ACTION_GET_CONTENT);
                filePicker.setType("video/mp4");
                startActivityForResult(filePicker, 1);
            }
        });
    }

    private void loadFFMpegBinary() {
        try {
            ffmpeg = FFmpeg.getInstance(this);
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {
                @Override
                public void onFailure() {
                    showUnsupportedExceptionDialog();
                }
            });
        } catch (FFmpegNotSupportedException e) {
            showUnsupportedExceptionDialog();
        }
    }

    private void showUnsupportedExceptionDialog() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(getString(R.string.device_not_supported))
                .setMessage(getString(R.string.device_not_supported_message))
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .create()
                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                relativeLayout.setVisibility(View.VISIBLE);
                filePath = getPath(this,data.getData());
                String fileName = filePath.substring(filePath.lastIndexOf("/")+1,filePath.lastIndexOf("."));
                String folderName = Environment.getExternalStorageDirectory().getPath() + "/SubGen/"+fileName;
                if(!makeDirectory(folderName)){
                    relativeLayout.setVisibility(View.INVISIBLE);
                    return;
                }
                extractAudio(filePath,folderName);
            }
        }
    }

    private void extractAudio(String filePath, final String folderName) {
        textView.setText("Extracting Audio....");
        String cmd = "-i "+filePath+" "+folderName+"/"+"video1.flac";
        final String[] command = cmd.split(" ");
        if (command.length != 0) {
            try {
                ffmpeg.execute(command, new ExecuteBinaryResponseHandler() {
                    @Override
                    public void onFailure(String s) {
                        relativeLayout.setVisibility(View.INVISIBLE);
                        Toast.makeText(MainActivity.this, "FAILED with output : " + s, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(String s) {
                    }

                    @Override
                    public void onProgress(String s) {
                    }

                    @Override
                    public void onStart() {
                    }

                    @Override
                    public void onFinish() {
                        channelManipulation(folderName);
                    }
                });
            } catch (FFmpegCommandAlreadyRunningException e) {
                relativeLayout.setVisibility(View.INVISIBLE);
                Log.e(TAG, "extractAudio: "+e.getLocalizedMessage() );
            }
        }
    }

    private void channelManipulation(final String folderName) {
        textView.setText("Manipulating audio channels ");
        String cmd = "-i "+folderName+"/"+"video1.flac -ac 1 "+folderName+"/"+"video2.flac";
        String[] command = cmd.split(" ");
        if (command.length != 0) {
            try {
                ffmpeg.execute(command, new ExecuteBinaryResponseHandler() {
                    @Override
                    public void onFailure(String s) {
                        relativeLayout.setVisibility(View.INVISIBLE);
                        Toast.makeText(MainActivity.this, "FAILED with output : " + s, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(String s) {
                    }

                    @Override
                    public void onProgress(String s) {
                    }

                    @Override
                    public void onStart() {
                    }

                    @Override
                    public void onFinish() {
                        segmentation(folderName);
                    }
                });
            } catch (FFmpegCommandAlreadyRunningException e) {
                relativeLayout.setVisibility(View.INVISIBLE);
                Log.e(TAG, "extractAudio: " + e.getLocalizedMessage());
            }
        }
    }

    private void segmentation(final String folderName) {
        textView.setText("Segmenting Video");
        String cmd = "-i "+folderName+"/"+"video2.flac -f segment -segment_time 3 -c copy "+folderName+"/"+"out%02d.flac";
        String[] command = cmd.split(" ");
        if (command.length != 0) {
            try {
                ffmpeg.execute(command, new ExecuteBinaryResponseHandler() {
                    @Override
                    public void onFailure(String s) {
                        relativeLayout.setVisibility(View.INVISIBLE);
                        Toast.makeText(MainActivity.this, "FAILED with output : " + s, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(String s) {
                    }

                    @Override
                    public void onProgress(String s) {
                    }

                    @Override
                    public void onStart() {
                    }

                    @Override
                    public void onFinish() {
                        try {
                            subtitileGeneration(folderName);
                        } catch (IOException e) {
                            relativeLayout.setVisibility(View.INVISIBLE);
                            e.printStackTrace();
                        }
                    }
                });
            } catch (FFmpegCommandAlreadyRunningException e) {
                relativeLayout.setVisibility(View.INVISIBLE);
                Log.e(TAG, "extractAudio: " + e.getLocalizedMessage());
            }
        }
    }

    private boolean makeDirectory(String folderName) {
        File dir = new File(folderName);
        try{
            if(dir.mkdir()) return true;
        }catch(Exception e){
            Toast.makeText(this, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
        }
        Toast.makeText(this, "Folder not Created", Toast.LENGTH_LONG).show();
        return false;
    }



    private void subtitileGeneration(final String folderName) throws IOException {
        textView.setText("Generating Subtitile...");
        final int n=new File(folderName).listFiles().length-2;
        final int j=n;

        String fpath = folderName+"/subgen.srt";

        File fileSrt = new File(fpath);

        if (!fileSrt.exists()) {
            fileSrt.createNewFile();
        }

        FileWriter fw = new FileWriter(fileSrt.getAbsoluteFile());
        final BufferedWriter bw = new BufferedWriter(fw);

        for(int i=0;i<n;i++){
            final File file = new File(folderName+"/out"+String.format("%02d", i)+".flac");
            if (file.exists()) {
                final int finalI = i;
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Uri soundUri = Uri.fromFile(file);
                            InputStream stream = getContentResolver().openInputStream(soundUri);
                            byte[] audioData = IOUtils.toByteArray(stream);
                            stream.close();

                            String base64EncodedData =
                                    Base64.encodeBase64String(audioData);

                            Speech speechService = new Speech.Builder(
                                    AndroidHttp.newCompatibleTransport(),
                                    new AndroidJsonFactory(),
                                    null
                            ).setSpeechRequestInitializer(
                                    new SpeechRequestInitializer(CLOUD_API_KEY))
                                    .build();
                            RecognitionConfig recognitionConfig = new RecognitionConfig();
                            recognitionConfig.setLanguageCode("en-US");
                            RecognitionAudio recognitionAudio = new RecognitionAudio();
                            recognitionAudio.setContent(base64EncodedData);

                            SyncRecognizeRequest request = new SyncRecognizeRequest();
                            request.setConfig(recognitionConfig);
                            request.setAudio(recognitionAudio);

                            SyncRecognizeResponse response = speechService.speech()
                                    .syncrecognize(request)
                                    .execute();

                            if (response.getResults()!=null) {
                                SpeechRecognitionResult result = response.getResults().get(0);
                                final String transcript = result.getAlternatives().get(0).getTranscript();
                                int s1= finalI *3;
                                int s2=s1+3;
                                int m1=s1/60;
                                int m2=s2/60;
                                s1%=60;
                                s2%=60;
                                bw.write((finalI+1)+"\n00:"+String.format("%02d", m1)+":"+String.format("%02d", s1)
                                        +",100 --> 00:"+String.format("%02d", m2)+":"+String.format("%02d", s2)
                                        +",000\n"+transcript+"\n\n" );
                            }
                            int per= ((finalI+1)*100)/n;
                            progressBar.setProgress(per);
                            if(finalI==n-1) {
                                bw.close();

                                Intent intent = new Intent(MainActivity.this,PlayerActivity.class);
                                intent.putExtra("video",filePath);
                                intent.putExtra("srt",folderName);
                                startActivity(intent);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                });
            }
        }

    }


    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    Log.d(TAG, "getPath: 1");
                    return Environment.getExternalStorageDirectory().getPath() + "/" + split[1];
                }
            } else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri;
                contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                Log.d(TAG, "getPath: 2");
                return getDataColumn(context, contentUri, null, null);
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {
                        split[1]
                };
                Log.d(TAG, "getPath: 3");
                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        } else if ("content".equalsIgnoreCase(uri.getScheme())) {
            if (isGooglePhotosUri(uri)){
                Log.d(TAG, "getPath: 4");
                return uri.getLastPathSegment();
            }
            Log.d(TAG, "getPath: 5");
            return getDataColumn(context, uri, null, null);
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            Log.d(TAG, "getPath: 6");
            return uri.getPath();
        }

        Log.d(TAG, "getPath: 7");
        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }
}
